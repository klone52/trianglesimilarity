from typing import List, Tuple
import math

Point = Tuple[int, int]
Coords = List[Point]

def fast_euclidian_distance(point1: Point, point2: Point) -> float:
    '''
    Input: Pair of pints (x, y)  

    Output: Euclidian distance between points (float)
    '''
    distance = abs(math.sqrt((point2[0]-point1[0])**2+(point2[1]-point1[1])**2))
    return distance

def get_sides_length(coords_1: Coords) -> List[float]:
    '''
    Input: List of tuples representig points 

    Output: List of length between points in the list
    '''
    sides_length: List[float] = []
    for idx in range(len(coords_1)):
        point1 = coords_1[idx-1]
        point2 = coords_1[idx]
        sides_length.append(fast_euclidian_distance(point2, point1))
    return sides_length

def get_sorted_sides_length(coords_1: Coords) -> List[float]:
    '''
    Input: List of tuples representig points 

    Output: Sorted List of length between points in the list
    '''
    sides_length: List[float] = []
    for idx in range(len(coords_1)):
        point1 = coords_1[idx-1]
        point2 = coords_1[idx]
        sides_length.append(fast_euclidian_distance(coords_1[idx], coords_1[idx-1]))
    return sorted(sides_length)

def check_length_proportion(sides1: List[float], sides2: List[float]) -> List[float]:
    '''
        Input: 2 List of float, of the same length
        
        Output: List of proportions found between corresponding elementes in both list
    '''
    proportions_found: List[float] = []
    for idx in range(len(sides1)):
        proportions_found.append(sides1[idx]/sides2[idx])

    return proportions_found

def get_line_angle(point1: Point, point2: Point) -> float:
    ''' Input:
            Pair of Tuple[int, int] that describe 1 line
        Output:
            Angle of line in degrees
    '''
    x1, y1 = point1
    x2, y2 = point2
    return math.degrees(math.atan2(y2-y1, x2-x1))

def get_triangle_angles(coords_1: Coords) -> List[float]:
    ''' Input: 
            list of tuples with 3 points describing a triangle   
        Output:
            List of angles for each corner in degrees, in the same order as input coords
    '''
    angles: List[float] = []
    for idx in range(len(coords_1)):
        adjacent_line_angle1 = get_line_angle(coords_1[idx-3], coords_1[idx-2])
        adjacent_line_angle2 = get_line_angle(coords_1[idx-3], coords_1[idx-1])
        
        corner_angle = abs(adjacent_line_angle2-adjacent_line_angle1)
        angles.append(corner_angle)
    return angles

def AAA_similarity(coords_1: Coords, coords_2: Coords) -> bool:
    ''' 
        Given 2 triangles check similarity with the method AAA
        
        Input: List[Tuple[int, int]], List[Tuple[int, int]]
        Output: Bool
    '''
    triangle1_angles = get_triangle_angles(coords_1)
    triangle2_angles = get_triangle_angles(coords_2)

    if sorted(triangle1_angles) == sorted(triangle2_angles):
            similar = True
    else:
            similar = False

    return similar

def SSS_similarity(coords_1: Coords, coords_2: Coords) -> bool:
    ''' 
        Given 2 triangles check similarity with the method SSS
        
        Input: List[Tuple[int, int]], List[Tuple[int, int]]
        Output: Bool
    '''

    sides_length_triangle1 = get_sorted_sides_length(coords_1)
    sides_length_triangle2 = get_sorted_sides_length(coords_2)

    proportions = check_length_proportion(sides_length_triangle1, 
                                            sides_length_triangle2)
    if len(set(proportions)) == 1:
        similar = True
    else:
        similar = False

    return similar

def SAS_similarity(coords_1: Coords, coords_2: Coords) -> bool:
    ''' 
        Given 2 triangles check similarity with the method SAS
        
        Input: List[Tuple[int, int]], List[Tuple[int, int]]
        Output: Bool
    '''
    
    triangle1_angles = get_triangle_angles(coords_1)
    triangle2_angles = get_triangle_angles(coords_2)
    similar = False

    for idx1, angle1 in enumerate(triangle1_angles):
        for idx2, angle2 in enumerate(triangle2_angles):
            propotions: List[float] = []
            triangle1_pair: List[float] = []
            triangle2_pair: List[float] = []
            if angle1 == angle2:
                for i in range(1,3):
                    triangle1_pair.append(fast_euclidian_distance(coords_1[idx1], coords_1[idx1-i]))
                    triangle2_pair.append(fast_euclidian_distance(coords_2[idx2], coords_2[idx2-i]))

                propotions = check_length_proportion(sorted(triangle1_pair), sorted(triangle2_pair))

                if len(set(propotions)) == 1:          
                    return True
                else:
                    continue
    return similar



def similar_triangles(coords_1: Coords, coords_2: Coords) -> bool:
    # write  your code here :)
    ''' Triangle Similarity function:
        Given 2 Triangles they are similar if: 
            - AAA: All three pairs of corresponding angles are the same.
            - SSS: All three pairs of corresponding sides are in the same proportion
            - SAS: Two pairs of sides in the same proportion and the included angle equal.
        
        ** Important ** Given all the coordinates of both triangles, we can check with AAA or SSS directly, 
        All method are included to extend the code review.
                
        Input: 
            List of Tuples describing 3 point for each triangle
        Output:
            Boolean: returns similarity True if last 2 conditions apply 
    '''     

    if SSS_similarity(coords_1, coords_2) or AAA_similarity(coords_1, coords_2) or SAS_similarity(coords_1, coords_2):
        areSimilar = True
    else:
        areSimilar = False
        
    return areSimilar


if __name__ == '__main__':    
    assert similar_triangles([(0, 0), (1, 2), (2, 0)], [(3, 0), (4, 2), (5, 0)]) is True
    assert similar_triangles([(0, 0), (1, 2), (2, 0)], [(3, 0), (4, 3), (5, 0)]) is False
    assert similar_triangles([(0, 0), (1, 2), (2, 0)], [(2, 0), (4, 4), (6, 0)]) is True
    assert similar_triangles([(0, 0), (0, 3), (2, 0)], [(3, 0), (5, 3), (5, 0)]) is True
    assert similar_triangles([(1, 0), (1, 2), (2, 0)], [(3, 0), (5, 4), (5, 0)]) is True
    assert similar_triangles([(1, 0), (1, 3), (2, 0)], [(3, 0), (5, 5), (5, 0)]) is False
